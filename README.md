# FEATools.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://stefano-tronci.gitlab.io/FEATools.jl/dev)
[![Build Status](https://gitlab.com/stefano-tronci/FEATools.jl/badges/master/pipeline.svg)](https://gitlab.com/stefano-tronci/FEATools.jl/pipelines)
[![Coverage](https://gitlab.com/stefano-tronci/FEATools.jl/badges/master/coverage.svg)](https://gitlab.com/stefano-tronci/FEATools.jl/commits/master)

Julia functions to aid pre- and post- processing of FEA studies using ElmerFEM and ParaView.

# Description

This package implements a handful of helper function that can aid postprocessing of output data from [Elmer FEM](http://www.elmerfem.org/blog/) and preparation of files for visualisation with [ParaView](https://www.paraview.org/). This package is also contains function to load Elmer solutions in Julia and postprocess the results with Julia.

## Should I use this Package?

This package exists mainly for the author personal convenience and it will be developed on a as-needed basis. You are welcome to use this package, but you should perhaps not base your projects on it.

# Installation

`PyCall` is included as dependency to this package but it might be tricky to install. For his own applications, the author found easiest and most beneficial to setup a python virtual environment dedicated to Julia. This environment is to be equipped with [meshio](https://pypi.org/project/meshio/). After this step is done the installation of this package should be smooth.

Refer to the step below to setup your Julia python virtual environment from scratch if you did not do so yet.

```bash
# Setting up a Python venv
python3 -m venv juliaenv
# Installing Plotly in the venv
juliaenv/bin/pip3 install meshio
```

Now, install `PyCall` by setting the `ENV["PYTHON"]` to the python binary in the newly created python virtual environment.

```julia
julia> ENV["PYTHON"]="~/juliaenv/bin/python3"
```
Press `]` to enter package mode and install and build PyCall:

```julia
(@v1.5) pkg> add PyCall
(@v1.5) pkg> build PyCall
```

From now on `PyCall` will use python from the `juliaenv`. If python packages need to be used through `PyCall`, these need to be installed in `juliaenv`.

This package can now be easily installed:

```julia
(@v1.5) pkg> add https://gitlab.com/computational-acoustics/featools.jl.git
```


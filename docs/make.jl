using FEATools
using Documenter

makedocs(;
    modules=[FEATools],
    authors="Stefano Tronci <tronci.stefano@gmail.com>",
    repo="https://gitlab.com/stefano-tronci/FEATools.jl/blob/{commit}{path}#L{line}",
    sitename="FEATools.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://stefano-tronci.gitlab.io/FEATools.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

using DelimitedFiles


"""
    write_paraview_time_array(x, path, fileName)
    
Write a 1D Julia array x into a collection of CSV files in the given path that can be read by ParaView as a time series.

The file name of the CSV files is given by concatenating the path with the fileName variable and appending _n.csv for each element of the array.
"""
function write_paraview_time_array(
    x::AbstractArray,
    path::String,
    fileName::String
    )

    for n in 1:length(x)
        writedlm(joinpath(path, fileName * "_$n.csv"), x[n])
    end

end

"""
    parse_convergence(fileName)
    
Parse the log file from ElmerFEM and extract convergence information. Currently supporting only the Helmholtz Solver, solved with linear iterations only. Returns a dictionary populated with a matrix for each value of frequency of the study. The first column of the matrix is the iteration number, the second is the convergence metric value.
"""
function parse_convergence(fileName::String)

  file = open(fileName)
  lines = readlines(file)
  close(file)

  frequencyMask = occursin.(
    "HelmholtzSolve:  Frequency (Hz):",
    lines
  )

  itStartMask = occursin.(
    "IterSolver: Calling complex iterative solver",
    lines
  )

  itEndMask = occursin.(
    "ComputeChange: NS (ITER=1)",
    lines
  ) .| occursin.(
    "NUMERICAL ERROR:: IterSolve: Too many iterations was needed.",
    lines
  )

  lineNumber = 1:length(lines)
  nSolutions = length(lines[frequencyMask])

  data = Dict()

  for n in 1:nSolutions

    freq = parse(Float64, split(lines[frequencyMask][n])[end])

    convLines = split.(
      lines[
        (lineNumber[itStartMask][n] + 1):(lineNumber[itEndMask][n] - 1)
      ]
    )

    it = [parse.(Float64, convLines[n])[1] for n in 1:length(convLines)]
    v = [parse.(Float64, convLines[n])[2] for n in 1:length(convLines)]

    convData = [it v]

    data[freq] = convData

  end

  return data

end

"""
    read_fem_eigenfrequencies(file_name)
    
Read eigenvalues from a .dat file output from ElmerFEM and return the associated eigenfrequencies in Hz.
Replace negative eigenvalues with zeros.
"""
function read_fem_eigenfrequencies(file_name::String)
    # Read, flatten the array and transform eigenvalues in eigenfrequencies.
    # Negative eigenvalues are artefacts and are replaced with zeros.
    data= readdlm(file_name)[:]
    data[data .< 0] .= 0
    return sqrt.(data) / 2π
end

"""
    load_mesh(file_name)
    
Load a VTU file output from ElmerFEM.
"""
function load_mesh(file_name::String)
    return meshio.read(file_name)
end

"""

    write_mesh(file_name, mesh)
    
Write the contents of meshio mesh object to file.
"""
function write_mesh(file_name::String, mesh::PyCall.PyObject)
    meshio.write(file_name, mesh)
end


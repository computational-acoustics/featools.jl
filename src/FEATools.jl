# Init code inspired by https://discourse.julialang.org/t/calling-the-constructor-outside-a-module-works-but-inside-a-module-throws-argumenterror-ref-of-null-pyobject/42362/4

__precompile__() # this module is safe to precompile
module FEATools

import PyCall

const meshio = PyCall.PyNULL()

function __init__()
    copy!(meshio, PyCall.pyimport("meshio"))
end

include("libFEATools.jl")

export write_paraview_time_array
export parse_convergence
export read_fem_eigenfrequencies
export load_mesh
export write_mesh

end
